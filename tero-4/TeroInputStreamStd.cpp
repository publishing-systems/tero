/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroInputStreamStd.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-14
 */


#include "TeroInputStreamStd.h"


namespace tero
{

TeroInputStreamStd::TeroInputStreamStd(std::istream& aStream):
  m_aStream(aStream)
{

}

TeroInputStreamStd::~TeroInputStreamStd()
{

}

int TeroInputStreamStd::get(char& cCharacter)
{
    if (m_nBufferIndex == std::string::npos)
    {
        m_aStream.get(cCharacter);
    }
    else
    {
        cCharacter = m_strBuffer.at(m_nBufferIndex);
        ++m_nBufferIndex;

        if (m_nBufferIndex >= m_strBuffer.size())
        {
            m_strBuffer = "";
            m_nBufferIndex = std::string::npos;
        }
    }

    return 0;
}

bool TeroInputStreamStd::eof()
{
    if (m_nBufferIndex == std::string::npos)
    {
        return m_aStream.eof();
    }
    else
    {
        return false;
    }
}

bool TeroInputStreamStd::bad()
{
    if (m_nBufferIndex == std::string::npos)
    {
        return m_aStream.bad();
    }
    else
    {
        return false;
    }
}

int TeroInputStreamStd::push(const std::string& strBuffer)
{
    if (strBuffer.empty() == true)
    {
        return 0;
    }

    m_strBuffer += strBuffer;

    if (m_nBufferIndex == std::string::npos)
    {
        m_nBufferIndex = 0U;
    }

    return 0;
}

}
