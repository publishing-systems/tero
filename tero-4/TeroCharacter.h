/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroCharacter.h
 * @author Stephan Kreutzer
 * @since 2021-09-16
 */

#ifndef _TERO_TEROCHARACTER_H
#define _TERO_TEROCHARACTER_H


#include "TeroRule.h"


namespace tero
{

class TeroCharacter : public TeroRule
{
public:
    TeroCharacter(const char& cCharacter);

public:
    int Compare(const char& cByte);
    int Reset();

protected:
    char m_cCharacter;

};

}

#endif
