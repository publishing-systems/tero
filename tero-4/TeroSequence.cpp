/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroSequence.cpp
 * @author Stephan Kreutzer
 * @since 2021-09-16
 */


#include "TeroSequence.h"


namespace tero
{

TeroSequence::TeroSequence(const std::string& strSequence):
  m_strSequence(strSequence)
{

}

int TeroSequence::Compare(const char& cByte)
{
    if (m_nComparisonPosition == std::string::npos)
    {
        m_nComparisonPosition = 0U;
    }
    else if (m_nComparisonPosition == m_strSequence.length())
    {
        return TeroRule::RETURNVALUE_COMPARE_MATCH;
    }

    if (cByte != m_strSequence.at(m_nComparisonPosition))
    {
        m_nComparisonPosition = std::string::npos;
        return TeroRule::RETURNVALUE_COMPARE_MISMATCH;
    }
    else
    {
        m_nComparisonPosition += 1U;

        if (m_nComparisonPosition == m_strSequence.length())
        {
            return TeroRule::RETURNVALUE_COMPARE_MATCH;
        }
        else
        {
            return TeroRule::RETURNVALUE_COMPARE_MATCHING;
        }
    }
}

int TeroSequence::Reset()
{
    m_nComparisonPosition = std::string::npos;
    return 0;
}

}
