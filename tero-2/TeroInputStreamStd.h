/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroInputStreamStd.h
 * @author Stephan Kreutzer
 * @since 2021-06-14
 */

#ifndef _TERO_TEROINPUTSTREAMSTD_H
#define _TERO_TEROINPUTSTREAMSTD_H

#include "TeroInputStreamInterface.h"
#include <istream>

namespace tero
{

class TeroInputStreamStd : public TeroInputStreamInterface
{
public:
    TeroInputStreamStd(std::istream& aStream);
    ~TeroInputStreamStd();

public:
    int get(char& cCharacter);
    bool eof();
    bool bad();

protected:
    std::istream& m_aStream;

};

}

#endif
