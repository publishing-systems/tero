/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroInterpreter.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-23
 */

#include "TeroInterpreter.h"
#include "TeroPattern.h"
#include <sstream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <unordered_set>
/** @todo remove? */
#include <iostream>

namespace tero
{

TeroInterpreter::TeroInterpreter(const std::string& strCodeDirectoryPath,
                                 const std::string& strMainFunction):
  m_strCodeDirectoryPath(strCodeDirectoryPath),
  m_strCurrentFunction(strMainFunction)
{
    if (m_strCurrentFunction.empty() == true)
    {
        throw new std::runtime_error("Main function name is empty.");
    }
}

TeroInterpreter::~TeroInterpreter()
{

}

std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator TeroInterpreter::LoadCode()
{
    std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator iterFunction = m_aFunctions.find(m_strCurrentFunction);

    if (iterFunction != m_aFunctions.end())
    {
        return iterFunction;
    }

    std::string strCodeFilePath;
    strCodeFilePath += m_strCodeDirectoryPath;
    strCodeFilePath += m_strCurrentFunction;
    strCodeFilePath += ".tero";

    std::vector<std::unique_ptr<std::string>> aSections;

    std::unique_ptr<std::ifstream> pCodeStream = nullptr;

    try
    {
        pCodeStream = std::unique_ptr<std::ifstream>(new std::ifstream);
        pCodeStream->open(strCodeFilePath, std::ios::in | std::ios::binary);

        if (pCodeStream->is_open() != true)
        {
            std::stringstream aMessage;
            aMessage << "Couldn't open input file \"" << strCodeFilePath << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        char cByte = '\0';

        do
        {
            pCodeStream->get(cByte);

            if (pCodeStream->eof() == true)
            {
                break;
            }

            if (pCodeStream->bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            if (cByte == '(')
            {
                std::unique_ptr<std::string> pSection = nullptr;

                HandleSection(*pCodeStream, pSection);

                aSections.push_back(std::move(pSection));
            }
            else
            {
                /*
                int nByte(cByte);
                std::stringstream aMessage;
                aMessage << "Unexpected character '" << cByte << "' (0x"
                        << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                        << ").";
                throw new std::runtime_error(aMessage.str());
                */
            }

        } while (true);

        pCodeStream->close();
        pCodeStream.reset(nullptr);
    }
    catch (std::exception* pException)
    {
        if (pCodeStream != nullptr)
        {
            if (pCodeStream->is_open() == true)
            {
                pCodeStream->close();
            }
        }

        throw pException;
    }

    std::size_t nSectionCount = aSections.size();

    if (nSectionCount < 6U)
    {
        throw new std::runtime_error("Less than the minimum total of at least 6 sections.");
    }

    {
        int nSectionCountThenCases = (nSectionCount - 3U);

        if (nSectionCountThenCases % 3U != 0U)
        {
            throw new std::runtime_error("Then-case incomplete, section(s) missing.");
        }
    }

    if (aSections.at(0)->empty() == true)
    {
        throw new std::runtime_error("Section 1 is empty.");
    }

    if (*(aSections.at(0)) != m_strCurrentFunction)
    {
        std::stringstream aMessage;
        aMessage << "Function name \""
                 << *(aSections.at(0))
                 << "\" does mismatch expected code file name \""
                 << m_strCurrentFunction
                 << "\" of path \""
                 << strCodeFilePath
                 << "\".";
        throw new std::runtime_error(aMessage.str());
    }

    std::unique_ptr<std::list<std::unique_ptr<TeroPattern>>> pPatterns(new std::list<std::unique_ptr<TeroPattern>>);
    std::unordered_set<std::string> aPatternList;

    for (std::size_t i = 1U, max = (nSectionCount - 2U); i < max; i += 3U)
    {
        if (aSections.at(i)->empty() == true)
        {
            std::stringstream aMessage;
            aMessage << "In function \"" << *(aSections.at(0)) << "\", section " << i << " is empty.";
            throw new std::runtime_error(aMessage.str());
        }

        if (aPatternList.find(*(aSections.at(i))) == aPatternList.end())
        {
            aPatternList.insert(*(aSections.at(i)));
        }
        else
        {
            std::stringstream aMessage;
            aMessage << "In function \"" << *(aSections.at(0)) << "\", pattern \"" << *(aSections.at(i)) << "\" is defined more than once.";
            throw new std::runtime_error(aMessage.str());
        }

        if (aSections.at(i + 2U)->empty() == true)
        {
            std::stringstream aMessage;
            aMessage << "In function \"" << *(aSections.at(0)) << "\", section " << (i + 2U) << " is empty.";
            throw new std::runtime_error(aMessage.str());
        }

        /** @todo Pass/move std::unique_ptr<std::string> into TeroPattern instead of copying? */
        std::unique_ptr<TeroPattern> pPattern(new TeroPattern(*(aSections.at(i)),
                                                              *(aSections.at(i + 1U)),
                                                              *(aSections.at(i + 2U))));

        pPatterns->push_back(std::move(pPattern));
    }

    if (aSections.at(nSectionCount - 1U)->empty() == true)
    {
        std::stringstream aMessage;
        aMessage << "In function \"" << *(aSections.at(0)) << "\", section " << (nSectionCount - 1U) << " is empty.";
        throw new std::runtime_error(aMessage.str());
    }

    std::unique_ptr<TeroFunction> pFunction(new TeroFunction(*(aSections.at(0)),
                                                             std::move(pPatterns),
                                                             *(aSections.at(nSectionCount - 2U)),
                                                             *(aSections.at(nSectionCount - 1U))));

    return m_aFunctions.insert(std::pair<std::string, std::unique_ptr<TeroFunction>>(*(aSections.at(0)), std::move(pFunction))).first;
}

int TeroInterpreter::HandleSection(std::istream& aStream,
                                   std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("TeroInterpreter::HandleSection() with pResult != nullptr.");
    }

    std::stringstream strSection;
    char cByte = '\0';

    do
    {
        aStream.get(cByte);

        if (aStream.eof() == true)
        {
            throw new std::runtime_error("Unexpected end of stream while attempting to read the section.");
        }

        if (aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == ')')
        {
            pResult.reset(new std::string);
            *pResult = strSection.str();
            return 0;
        }
        else
        {
            strSection << cByte;
        }

    } while (true);
}

/**
 * @param[in] aStream Will get consumed!
 */
int TeroInterpreter::ParseInput(TeroInputStreamInterface& aStream)
{
    do
    {
        if (Parse(aStream) != 0)
        {
            break;
        }

    } while (true);

    return 0;
}

int TeroInterpreter::Parse(TeroInputStreamInterface& aStream)
{
    std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator iterFunction = LoadCode();

    if (iterFunction == m_aFunctions.end())
    {
        std::stringstream aMessage;
        aMessage << "Call of unknown function \"" << m_strCurrentFunction << "\".";
        throw new std::runtime_error(aMessage.str());
    }

    const std::list<std::unique_ptr<TeroPattern>>& aThenCasePatterns = iterFunction->second->GetThenCasePatterns();

    /** @todo The matching of multiple patterns itself is probably very inefficiently implemented,
      * but initially left this way for clarity of and illustrating the concept. State automaton
      * could stay at the current state/function if no match/transition is reached. */
    std::map<std::size_t, std::string> aPatternList;

    {
        std::size_t i = 0U;

        for (std::list<std::unique_ptr<TeroPattern>>::const_iterator iter = aThenCasePatterns.begin();
             iter != aThenCasePatterns.end();
             iter++)
        {
            aPatternList.insert(std::pair<std::size_t, std::string>(i, (*iter)->GetPattern()));
            ++i;
        }
    }


    std::string strBuffer;
    char cByte = '\0';
    std::map<std::size_t, std::string>::const_iterator iterMatch = aPatternList.end();

    do
    {
        /** @todo Trying to match the current patterns consumes input, no way to keep/forward yet! */
        aStream.get(cByte);

        if (aStream.eof() == true)
        {
            return 1;
        }

        if (aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        strBuffer += cByte;

        for (std::map<std::size_t, std::string>::const_iterator iter = aPatternList.begin();
              iter != aPatternList.end();
              iter++)
        {
            if (iter->second.at(strBuffer.length() - 1U) == cByte)
            {
                if (iter->second.length() == strBuffer.length())
                {
                    iterMatch = iter;
                    break;
                }
            }
            else
            {
                aPatternList.erase(iter);
            }
        }

        if (iterMatch != aPatternList.end() ||
            aPatternList.empty() == true)
        {
            break;
        }

    } while (true);


    if (iterMatch != aPatternList.end())
    {
        std::list<std::unique_ptr<TeroPattern>>::const_iterator iterThenCasePattern = aThenCasePatterns.end();

        {
            std::list<std::unique_ptr<TeroPattern>>::const_iterator iter = aThenCasePatterns.begin();

            for (std::size_t i = 0U, max = iterMatch->first; i <= max; i++)
            {
                if (i == max)
                {
                    iterThenCasePattern = iter;
                    break;
                }
                else
                {
                    iter++;
                }
            }
        }

        const std::string& strThenCaseCode = (*iterThenCasePattern)->GetThenCaseCode();

        /** @todo This is just a temporary quick solution (as it prevents literal dot). */
        if (strThenCaseCode == ".")
        {
            std::cout << strBuffer;
        }
        else
        {
            std::cout << strThenCaseCode;
        }

        /** @todo Make this pointer/reference, not copy! */
        m_strCurrentFunction = (*iterThenCasePattern)->GetThenCaseFunction();
    }
    else
    {
        const std::string& strElseCaseCode = iterFunction->second->GetElseCaseCode();

        /** @todo This is just a temporary quick solution (as it prevents literal dot). */
        if (strElseCaseCode == ".")
        {
            std::cout << strBuffer;
        }
        else
        {
            std::cout << strElseCaseCode;
        }

        /** @todo Make this pointer/reference, not copy! */
        m_strCurrentFunction = iterFunction->second->GetElseCaseFunction();
    }

    return 0;
}

}
