/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroPattern.h
 * @author Stephan Kreutzer
 * @since 2021-08-15
 */

#ifndef _TERO_TEROPATTERN_H
#define _TERO_TEROPATTERN_H


#include <string>
#include <stdexcept>


namespace tero
{

class TeroPattern
{
public:
    TeroPattern(const std::string& strPattern,
                const std::string& strThenCaseCode,
                const std::string& strThenCaseFunction);

public:
    const std::string& GetPattern();
    const std::string& GetThenCaseCode();
    const std::string& GetThenCaseFunction();

protected:
    std::string m_strPattern;
    std::string m_strThenCaseCode;
    std::string m_strThenCaseFunction;

};

}

#endif
