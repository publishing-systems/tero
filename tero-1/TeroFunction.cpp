/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroFunction.cpp
 * @author Stephan Kreutzer
 * @since 2021-07-18
 */


#include <stdexcept>
#include "TeroFunction.h"


namespace tero
{

TeroFunction::TeroFunction(const std::string& strName,
                           const std::string& strCondition,
                           const std::string& strThenCaseCode,
                           const std::string& strThenCaseFunction,
                           const std::string& strElseCaseCode,
                           const std::string& strElseCaseFunction):
  m_strName(strName),
  m_strCondition(strCondition),
  m_strThenCaseCode(strThenCaseCode),
  m_strThenCaseFunction(strThenCaseFunction),
  m_strElseCaseCode(strElseCaseCode),
  m_strElseCaseFunction(strElseCaseFunction)
{
    if (m_strName.empty() == true)
    {
        throw new std::invalid_argument("TeroFunction::TeroFunction(): Empty name string passed.");
    }

    if (m_strCondition.empty() == true)
    {
        throw new std::invalid_argument("TeroFunction::TeroFunction(): Empty condition string passed.");
    }

    if (m_strThenCaseFunction.empty() == true)
    {
        throw new std::invalid_argument("TeroFunction::TeroFunction(): Empty then-case function string passed.");
    }

    if (m_strElseCaseFunction.empty() == true)
    {
        throw new std::invalid_argument("TeroFunction::TeroFunction(): Empty else-case function string passed.");
    }
}

const std::string& TeroFunction::GetCondition()
{
    return m_strCondition;
}

const std::string& TeroFunction::GetThenCaseCode()
{
    return m_strThenCaseCode;
}

const std::string& TeroFunction::GetThenCaseFunction()
{
    return m_strThenCaseFunction;
}

const std::string& TeroFunction::GetElseCaseCode()
{
    return m_strElseCaseCode;
}

const std::string& TeroFunction::GetElseCaseFunction()
{
    return m_strElseCaseFunction;
}

}
