/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroFunction.h
 * @author Stephan Kreutzer
 * @since 2021-07-18
 */

#ifndef _TERO_TEROFUNCTION_H
#define _TERO_TEROFUNCTION_H


#include <string>
#include <stdexcept>


namespace tero
{

class TeroFunction
{
public:
    TeroFunction(const std::string& strName,
                 const std::string& strCondition,
                 const std::string& strThenCaseCode,
                 const std::string& strThenCaseFunction,
                 const std::string& strElseCaseCode,
                 const std::string& strElseCaseFunction);

public:
    const std::string& GetCondition();
    const std::string& GetThenCaseCode();
    const std::string& GetThenCaseFunction();
    const std::string& GetElseCaseCode();
    const std::string& GetElseCaseFunction();

protected:
    std::string m_strName;
    std::string m_strCondition;
    std::string m_strThenCaseCode;
    std::string m_strThenCaseFunction;
    std::string m_strElseCaseCode;
    std::string m_strElseCaseFunction;

};

}

#endif
