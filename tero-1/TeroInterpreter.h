/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroInterpreter.h
 * @author Stephan Kreutzer
 * @since 2021-06-23
 */

#ifndef _TERO_TEROINTERPRETER_H
#define _TERO_TEROINTERPRETER_H

#include <istream>
#include <string>
#include <locale>
#include <memory>
#include <map>
#include <stdexcept>
#include "TeroFunction.h"

namespace tero
{

class TeroInterpreter
{
public:
    TeroInterpreter(const std::string& strCodeDirectoryPath,
                    const std::string& strMainFunction);
    ~TeroInterpreter();

public:
    std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator LoadCode();
    int ParseInput(std::istream& aStream);

protected:
    int HandleSection(std::istream& aStream,
                      std::unique_ptr<std::string>& pResult);

protected:
    int Parse(std::istream& aStream);

protected:
    std::map<std::string, std::unique_ptr<TeroFunction>> m_aFunctions;

protected:
    std::string m_strCodeDirectoryPath;
    std::string m_strCurrentFunction;
    std::locale m_aLocale;

};

}

#endif
