/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroInterpreter.cpp
 * @author Stephan Kreutzer
 * @since 2021-06-23
 */

#include "TeroInterpreter.h"
#include <sstream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <iostream> /** @todo remove? */

namespace tero
{

TeroInterpreter::TeroInterpreter(const std::string& strCodeDirectoryPath,
                                 const std::string& strMainFunction):
  m_strCodeDirectoryPath(strCodeDirectoryPath),
  m_strCurrentFunction(strMainFunction)
{
    if (m_strCurrentFunction.empty() == true)
    {
        throw new std::runtime_error("Main function name is empty.");
    }
}

TeroInterpreter::~TeroInterpreter()
{

}

/**
 * @param[in] aStream Will get consumed!
 */
std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator TeroInterpreter::LoadCode()
{
    std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator iterFunction = m_aFunctions.find(m_strCurrentFunction);

    if (iterFunction != m_aFunctions.end())
    {
        return iterFunction;
    }

    std::string strCodeFilePath;
    strCodeFilePath += m_strCodeDirectoryPath;
    strCodeFilePath += m_strCurrentFunction;
    strCodeFilePath += ".tero";

    std::vector<std::unique_ptr<std::string>> aSections;
    int nSectionCount = 0;

    std::unique_ptr<std::ifstream> pCodeStream = nullptr;

    try
    {
        pCodeStream = std::unique_ptr<std::ifstream>(new std::ifstream);
        pCodeStream->open(strCodeFilePath, std::ios::in | std::ios::binary);

        if (pCodeStream->is_open() != true)
        {
            std::stringstream aMessage;
            aMessage << "Couldn't open input file \"" << strCodeFilePath << "\".";
            throw new std::runtime_error(aMessage.str());
        }

        char cByte = '\0';

        do
        {
            pCodeStream->get(cByte);

            if (pCodeStream->eof() == true)
            {
                break;
            }

            if (pCodeStream->bad() == true)
            {
                throw new std::runtime_error("Stream is bad.");
            }

            if (cByte == '(')
            {
                if (nSectionCount >= 6)
                {
                    throw new std::runtime_error("More than 6 sections encountered.");
                }

                std::unique_ptr<std::string> pSection = nullptr;

                HandleSection(*pCodeStream, pSection);
                ++nSectionCount;

                aSections.push_back(std::move(pSection));
            }
            else
            {
                /*
                int nByte(cByte);
                std::stringstream aMessage;
                aMessage << "Unexpected character '" << cByte << "' (0x"
                        << std::hex << std::uppercase << std::right << std::setfill('0') << std::setw(2) << nByte
                        << ").";
                throw new std::runtime_error(aMessage.str());
                */
            }

        } while (true);

        pCodeStream->close();
        pCodeStream.reset(nullptr);
    }
    catch (std::exception* pException)
    {
        if (pCodeStream != nullptr)
        {
            if (pCodeStream->is_open() == true)
            {
                pCodeStream->close();
            }
        }

        throw pException;
    }


    if (nSectionCount != 6 ||
        aSections.size() != 6)
    {
        throw new std::runtime_error("Less than 6 sections.");
    }

    if (aSections.at(0)->empty() == true)
    {
        throw new std::runtime_error("Section 1 is empty.");
    }

    if (*(aSections.at(0)) != m_strCurrentFunction)
    {
        std::stringstream aMessage;
        aMessage << "Function name \""
                 << *(aSections.at(0))
                 << "\" does mismatch expected code file name \""
                 << m_strCurrentFunction
                 << "\" of path \""
                 << strCodeFilePath
                 << "\".";
        throw new std::runtime_error(aMessage.str());
    }

    if (aSections.at(1)->empty() == true)
    {
        throw new std::runtime_error("Section 2 is empty.");
    }

    if (aSections.at(3)->empty() == true)
    {
        throw new std::runtime_error("Section 4 is empty.");
    }

    if (aSections.at(5)->empty() == true)
    {
        throw new std::runtime_error("Section 6 is empty.");
    }

    std::unique_ptr<TeroFunction> pFunction(new TeroFunction(*(aSections.at(0)),
                                                             *(aSections.at(1)),
                                                             *(aSections.at(2)),
                                                             *(aSections.at(3)),
                                                             *(aSections.at(4)),
                                                             *(aSections.at(5))));

    return m_aFunctions.insert(std::pair<std::string, std::unique_ptr<TeroFunction>>(*(aSections.at(0)), std::move(pFunction))).first;
}

int TeroInterpreter::HandleSection(std::istream& aStream,
                                   std::unique_ptr<std::string>& pResult)
{
    if (pResult != nullptr)
    {
        throw new std::invalid_argument("TeroInterpreter::HandleSection() with pResult != nullptr.");
    }

    std::stringstream strSection;
    char cByte = '\0';

    do
    {
        aStream.get(cByte);

        if (aStream.eof() == true)
        {
            throw new std::runtime_error("Unexpected end of stream while attempting to read the section.");
        }

        if (aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte == ')')
        {
            pResult.reset(new std::string);
            *pResult = strSection.str();
            return 0;
        }
        else
        {
            strSection << cByte;
        }

    } while (true);
}

/**
 * @param[in] aStream Will get consumed!
 */
int TeroInterpreter::ParseInput(std::istream& aStream)
{
    do
    {
        if (Parse(aStream) != 0)
        {
            break;
        }

    } while (true);

    return 0;
}

int TeroInterpreter::Parse(std::istream& aStream)
{
    std::map<std::string, std::unique_ptr<TeroFunction>>::const_iterator iterFunction = LoadCode();

    if (iterFunction == m_aFunctions.end())
    {
        std::stringstream aMessage;
        aMessage << "Call of unknown function \"" << m_strCurrentFunction << "\".";
        throw new std::runtime_error(aMessage.str());
    }

    const std::string strCondition = iterFunction->second->GetCondition();
    char cByte = '\0';
    bool bMatch = true;

    for (std::size_t i = 0, max = strCondition.length(); i < max; i++)
    {
        /** @todo Every function call reading a new character prevents multiple-choice condition chaining! */
        aStream.get(cByte);

        if (aStream.eof() == true)
        {
            return 1;
        }

        if (aStream.bad() == true)
        {
            throw new std::runtime_error("Stream is bad.");
        }

        if (cByte != strCondition.at(i))
        {
            bMatch = false;
            break;
        }
    }

    if (bMatch == true)
    {
        const std::string& strThenCaseCode = iterFunction->second->GetThenCaseCode();

        /** @todo This is just a temporary quick solution (as it prevents literal dot). */
        if (strThenCaseCode == ".")
        {
            std::cout << cByte;
        }
        else
        {
            std::cout << strThenCaseCode;
        }

        /** @todo Make this pointer/reference, not copy! */
        m_strCurrentFunction = iterFunction->second->GetThenCaseFunction();
    }
    else
    {
        const std::string& strElseCaseCode = iterFunction->second->GetElseCaseCode();

        /** @todo This is just a temporary quick solution (as it prevents literal dot). */
        if (strElseCaseCode == ".")
        {
            std::cout << cByte;
        }
        else
        {
            std::cout << strElseCaseCode;
        }

        /** @todo Make this pointer/reference, not copy! */
        m_strCurrentFunction = iterFunction->second->GetElseCaseFunction();
    }

    return 0;
}

}
