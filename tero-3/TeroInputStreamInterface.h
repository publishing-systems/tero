/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroInputStreamInterface.h
 * @author Stephan Kreutzer
 * @since 2021-06-13
 */

#ifndef _TERO_TEROINPUTSTREAMINTERFACE_H
#define _TERO_TEROINPUTSTREAMINTERFACE_H

#include <string>

namespace tero
{

class TeroInputStreamInterface
{
public:
    virtual int get(char& cCharacter) = 0;
    virtual bool eof() = 0;
    virtual bool bad() = 0;

public:
    virtual int push(const std::string& strBuffer) = 0;

};

}

#endif
