/* Copyright (C) 2021 Stephan Kreutzer
 *
 * This file is part of tero.
 *
 * tero is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License version 3 or any later
 * version of the license, as published by the Free Software Foundation.
 *
 * tero is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License 3 for more details.
 *
 * You should have received a copy of the GNU Affero General Public License 3
 * along with tero. If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * @file $/TeroPattern.cpp
 * @author Stephan Kreutzer
 * @since 2021-08-15
 */


#include <stdexcept>
#include "TeroPattern.h"


namespace tero
{

TeroPattern::TeroPattern(const std::string& strPattern,
                         const std::string& strThenCaseCode,
                         const std::string& strThenCaseFunction):
  m_strPattern(strPattern),
  m_strThenCaseCode(strThenCaseCode),
  m_strThenCaseFunction(strThenCaseFunction)
{
    if (m_strPattern.empty() == true)
    {
        throw new std::invalid_argument("TeroPattern::TeroPattern(): Empty pattern string passed.");
    }

    if (m_strThenCaseFunction.empty() == true)
    {
        throw new std::invalid_argument("TeroPattern::TeroPattern(): Empty then-case function string passed.");
    }
}

const std::string& TeroPattern::GetPattern()
{
    return m_strPattern;
}

const std::string& TeroPattern::GetThenCaseCode()
{
    return m_strThenCaseCode;
}

const std::string& TeroPattern::GetThenCaseFunction()
{
    return m_strThenCaseFunction;
}

}
