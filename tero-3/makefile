# Copyright (C) 2021  Stephan Kreutzer
#
# This file is part of tero.
#
# tero is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License version 3 or any later
# version of the license, as published by the Free Software Foundation.
#
# tero is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License 3 for more details.
#
# You should have received a copy of the GNU Affero General Public License 3
# along with tero. If not, see <http://www.gnu.org/licenses/>.



.PHONY: build clean



CXX = g++
CXXFLAGS = -std=c++11 -Wall -Werror -Wextra -pedantic



build: tero



OBJECTS = tero.o TeroInputStreamStd.o TeroPattern.o TeroFunction.o TeroInterpreter.o

.SUFFIXES: .cpp
.cpp.o:
	$(CXX) $< -c $(CXXFLAGS)



tero: $(OBJECTS)
	$(CXX) $(OBJECTS) -o $@ $(CXXFLAGS)

tero.o: tero.cpp TeroInterpreter.h
TeroInterpreter.o: TeroInterpreter.cpp TeroInterpreter.h
TeroFunction.o: TeroFunction.cpp TeroFunction.h
TeroPattern.o: TeroPattern.cpp TeroPattern.h
TeroInputStreamStd.o: TeroInputStreamStd.cpp TeroInputStreamStd.h

clean:
	rm -f tero $(OBJECTS)
